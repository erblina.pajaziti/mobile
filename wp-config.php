<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alb-kos' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7x6c$r~{rbV9ZXb#z(ZyW-FRRUljhdyFM1.LAXcLFuK<$Fi!:uK,@?#RTx,JHRUp' );
define( 'SECURE_AUTH_KEY',  'DwmOMLqS=!E%|VI0H^^ezK&1Jz/_9^9X28r ?}FN|=t(K!s]~K1<[8):L!Qen=1?' );
define( 'LOGGED_IN_KEY',    '}qRktBaQqVM5W<X%tn*cjuz9J9qm]CCFsq#e,z/32;TV07V5@yH9.`k046X<s@#2' );
define( 'NONCE_KEY',        '.G409bO7*ma5whsm|/ VH*;#G542xno MAh;&JU?kK<n.8:0F0tdi3-IzO{` <Uo' );
define( 'AUTH_SALT',        ']Jw>/G@9o;2QpK{IQcT-d2<EqEm@[;1^mIW]yH=Iw?0|GKAg9>)+qBV;CKQ0E!F:' );
define( 'SECURE_AUTH_SALT', '-*^xpbMAp:`,Pwh-%5[bQxa4(]XEC&}jkmp?cw+QonmA|tt!/f]iw(KUnTBQ|z]#' );
define( 'LOGGED_IN_SALT',   'lbz$qd*yN`o!*C8Rd3>@!K@eqWk,TAhnfFBt jjO+~o[#glEt([d`Y7DJf[-J6+*' );
define( 'NONCE_SALT',       '8aZ5~1ocu9EX7#_{L^:qsMn5|5[Nq`XdKSGC{4*G5zP@MlEK;ayvJ|t8RT0}S,1T' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
